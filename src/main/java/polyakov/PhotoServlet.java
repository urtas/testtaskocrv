package polyakov;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.sql.SQLException;


public class PhotoServlet extends HttpServlet {

    private static final int DEFAULT_BUFFER_SIZE = 10240;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {

        response.setContentType("image/gif");
        String requestParameter = request.getParameter("id");
        int id = Integer.parseInt(requestParameter);
        BufferedInputStream input = null;
        BufferedOutputStream output = null;

        response.reset();
        response.setBufferSize(DEFAULT_BUFFER_SIZE);


        try {
            input = new BufferedInputStream(new TrainDriverDao().getPhoto(id),DEFAULT_BUFFER_SIZE);
            output = new BufferedOutputStream(response.getOutputStream(),DEFAULT_BUFFER_SIZE);
            byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
            int length;
            while ((length = input.read(buffer)) > 0) {
                output.write(buffer, 0, length);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (input!=null)
                input.close();
            if (output!=null)
                output.close();
        }


    }
}

