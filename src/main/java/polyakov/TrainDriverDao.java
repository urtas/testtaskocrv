package polyakov;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

class TrainDriverDao {
    private static ApplicationContext context = new ClassPathXmlApplicationContext(new String[]{"springContext.xml"});

    private static BasicDataSource dataSource = context.getBean("dataSource", BasicDataSource.class);


    List<TrainDriver> getDrivers() throws SQLException {

        Connection connection = dataSource.getConnection();
        List<TrainDriver> drivers = new ArrayList<TrainDriver>();
        PreparedStatement preparedStatement = connection.prepareStatement("SELECT id, name, birthday, phone_number FROM postgres.main.person ORDER BY id");
        ResultSet resultSet = preparedStatement.executeQuery();

        while (resultSet.next()) {
            int id = resultSet.getInt("id");
            String name = resultSet.getString("name");
            Date birthday = resultSet.getDate("birthday");
            String phone_number = resultSet.getString("phone_number");
            drivers.add(new TrainDriver(id, name, birthday, phone_number));
        }

        resultSet.close();
        preparedStatement.close();
        connection.close();

        return drivers;

    }

    InputStream getPhoto(int id) throws SQLException {
        Connection connection = dataSource.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement("SELECT photo FROM postgres.main.person WHERE id=?");
        preparedStatement.setInt(1, id);

        ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
        InputStream photo = resultSet.getBinaryStream("photo");
        resultSet.close();
        preparedStatement.close();
        connection.close();
        return photo;
    }
}
