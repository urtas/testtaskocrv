package polyakov;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class TrainDriverServlet extends HttpServlet{

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<TrainDriver> drivers = null;
        try {
            drivers = new TrainDriverDao().getDrivers();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        request.setAttribute("trainDrivers", drivers);

        getServletContext().getRequestDispatcher("/trainDrivers.jsp").forward(request , response);

    }
}
