<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/resources/main.css" type="text/css"/>
        <title>Train drivers store</title>
    </head>
    <body>

        <table>
            <caption>Список машинистов.</caption>
            <tr>
                <th>ФИО</th>
                <th>Табельный номер</th>
                <th>Дата рождения</th>
                <th>Телефонный номер</th>
                <th>Фотография</th>
            </tr>

            <c:forEach items="${trainDrivers}" var="trainDriver">
                <tr>
                    <td>${trainDriver.name}</td>
                    <td>${trainDriver.id}</td>
                    <td>${trainDriver.birthday}</td>
                    <td>${trainDriver.phoneNumber}</td>
                    <td><img src="${pageContext.servletContext.contextPath}/getPhoto?id=${trainDriver.id}" height="100" onerror="this.style.visibility='hidden'"></td>
                </tr>
            </c:forEach>
        </table>
    </body>
</html>
